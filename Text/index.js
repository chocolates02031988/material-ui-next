'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Text = exports.default = undefined;

var _Text2 = require('./Text');

var _Text3 = _interopRequireDefault(_Text2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _Text3.default; /* eslint-disable flowtype/require-valid-file-annotation */

exports.Text = _Text3.default;