'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.IconButton = exports.default = undefined;

var _IconButton2 = require('./IconButton');

var _IconButton3 = _interopRequireDefault(_IconButton2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _IconButton3.default; /* eslint-disable flowtype/require-valid-file-annotation */

exports.IconButton = _IconButton3.default;