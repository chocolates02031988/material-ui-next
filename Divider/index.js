'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Divider = exports.default = undefined;

var _Divider2 = require('./Divider');

var _Divider3 = _interopRequireDefault(_Divider2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _Divider3.default; /* eslint-disable flowtype/require-valid-file-annotation */

exports.Divider = _Divider3.default;