'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Layout = exports.default = undefined;

var _Layout2 = require('./Layout');

var _Layout3 = _interopRequireDefault(_Layout2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _Layout3.default; /* eslint-disable flowtype/require-valid-file-annotation */

exports.Layout = _Layout3.default;