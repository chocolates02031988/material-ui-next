'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactDom = require('react-dom');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//  weak

var Portal = function (_Component) {
  (0, _inherits3.default)(Portal, _Component);

  function Portal() {
    var _ref;

    var _temp, _this, _ret;

    (0, _classCallCheck3.default)(this, Portal);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = Portal.__proto__ || (0, _getPrototypeOf2.default)(Portal)).call.apply(_ref, [this].concat(args))), _this), _this.layer = null, _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
  }

  (0, _createClass3.default)(Portal, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      this.renderLayer();
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate() {
      this.renderLayer();
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      this.unrenderLayer();
    }
  }, {
    key: 'getLayer',
    value: function getLayer() {
      return this.layer;
    }
  }, {
    key: 'unrenderLayer',
    value: function unrenderLayer() {
      if (!this.layer) {
        return;
      }
      (0, _reactDom.unmountComponentAtNode)(this.layer);
      if (document.body) {
        document.body.removeChild(this.layer);
      }
      this.layer = null;
    }
  }, {
    key: 'renderLayer',
    value: function renderLayer() {
      var _props = this.props,
          children = _props.children,
          open = _props.open;


      if (open) {
        if (!this.layer) {
          this.layer = document.createElement('div');
          this.layer.setAttribute('data-mui-portal', 'true');
          if (document.body) {
            document.body.appendChild(this.layer);
          }
        }

        /**
         * By calling this method in componentDidMount() and
         * componentDidUpdate(), you're effectively creating a "wormhole" that
         * funnels React's hierarchical updates through to a DOM node on an
         * entirely different part of the page.
         */

        var layerElement = _react2.default.Children.only(children);
        (0, _reactDom.unstable_renderSubtreeIntoContainer)(this, layerElement, this.layer);
      } else {
        this.unrenderLayer();
      }
    }
  }, {
    key: 'render',
    value: function render() {
      return null;
    }
  }]);
  return Portal;
}(_react.Component);

Portal.defaultProps = {
  open: false
};
exports.default = Portal;
process.env.NODE_ENV !== "production" ? Portal.propTypes = {
  /**
   * The content of the component.
   */
  children: _react.PropTypes.node,
  container: _react.PropTypes.oneOfType([_react.PropTypes.string, _react.PropTypes.func]),
  open: _react.PropTypes.bool
} : void 0;