'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Paper = exports.default = undefined;

var _Paper2 = require('./Paper');

var _Paper3 = _interopRequireDefault(_Paper2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _Paper3.default; /* eslint-disable flowtype/require-valid-file-annotation */

exports.Paper = _Paper3.default;