'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = createMixins;
//  weak

function createMixins(breakpoints, spacing) {
  function gutters(styles) {
    styles.paddingLeft = spacing.unit * 2;
    styles.paddingRight = spacing.unit * 2;
    styles[breakpoints.up('sm')] = {
      paddingLeft: spacing.unit * 3,
      paddingRight: spacing.unit * 3
    };
    return styles;
  }

  return { gutters: gutters };
}