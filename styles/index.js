'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.MuiThemeProvider = undefined;

var _MuiThemeProvider2 = require('./MuiThemeProvider');

var _MuiThemeProvider3 = _interopRequireDefault(_MuiThemeProvider2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.MuiThemeProvider = _MuiThemeProvider3.default; /* eslint-disable flowtype/require-valid-file-annotation */