'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Drawer = exports.default = undefined;

var _Drawer2 = require('./Drawer');

var _Drawer3 = _interopRequireDefault(_Drawer2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _Drawer3.default; /* eslint-disable flowtype/require-valid-file-annotation */

exports.Drawer = _Drawer3.default;