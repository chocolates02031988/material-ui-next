'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Icon = exports.default = undefined;

var _Icon2 = require('./Icon');

var _Icon3 = _interopRequireDefault(_Icon2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _Icon3.default; /* eslint-disable flowtype/require-valid-file-annotation */

exports.Icon = _Icon3.default;